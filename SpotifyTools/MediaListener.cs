﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.Control;

namespace SpotifyTools
{
    class MediaListener
    {

        DatabaseManager database;
        String currentArtist;
        String currentTrackName;
        String currentAlbum;
        double currentLength = 0;
        double currentListeningTime = 0;
        double startListeningTime = 0;
        bool songPaused = false;
        bool serviceRunning = true;
        GlobalSystemMediaTransportControlsSession currentGlobalSession;


        Action<String> updateStatusBar;

        public MediaListener(Action<String> callback)
        {
            database = new DatabaseManager();
            updateStatusBar = callback;
        }

        public void stopMediaListener() 
        { 
            serviceRunning = false;
            database.closeConnection();
             
            try
            { 
                currentGlobalSession.MediaPropertiesChanged -= callbackMediaProperties;
                currentGlobalSession.PlaybackInfoChanged -= callbackPlaybackStatus;
                currentGlobalSession.TimelinePropertiesChanged -= callbackTimelineStatus;
            }
            catch (Exception ex)
            { 
            }

            Console.WriteLine("Media server stopped");
        } 

        public void startMediaListener()
        {
            bool connResult = database.createConnection();

            if(connResult)
            { 
                GlobalSystemMediaTransportControlsSessionManager.RequestAsync().GetAwaiter().GetResult().CurrentSessionChanged += callbackSessionChanged;
                GlobalSystemMediaTransportControlsSession currentSession = GlobalSystemMediaTransportControlsSessionManager.RequestAsync().GetAwaiter().GetResult().GetCurrentSession();
                if (currentSession  == null || !currentSession.SourceAppUserModelId.ToLower().Contains("spotify"))
                    return;

                Console.WriteLine("Started");
                currentSession.MediaPropertiesChanged += callbackMediaProperties;
                currentSession.PlaybackInfoChanged += callbackPlaybackStatus;
                currentSession.TimelinePropertiesChanged += callbackTimelineStatus;

                currentGlobalSession = currentSession;

                serviceRunning = true;
            }
            else
            {
                serviceRunning = false;
            }
            
        } 

        private void callbackSessionChanged(GlobalSystemMediaTransportControlsSessionManager session, CurrentSessionChangedEventArgs e)
        {
            Console.WriteLine("Session changed" + session.GetCurrentSession().GetHashCode().ToString());
            

            if (session.GetCurrentSession() == null || !session.GetCurrentSession().SourceAppUserModelId.ToLower().Contains("spotify"))
                return;

            try
            { 
                currentGlobalSession.MediaPropertiesChanged -= callbackMediaProperties;
                currentGlobalSession.PlaybackInfoChanged -= callbackPlaybackStatus;
                currentGlobalSession.TimelinePropertiesChanged -= callbackTimelineStatus;
            }
            catch (Exception ex)
            {
            }

            session.GetCurrentSession().MediaPropertiesChanged += callbackMediaProperties;
            session.GetCurrentSession().PlaybackInfoChanged += callbackPlaybackStatus;
            session.GetCurrentSession().TimelinePropertiesChanged += callbackTimelineStatus;

            currentGlobalSession = session.GetCurrentSession();
        }

        private void callbackTimelineStatus(GlobalSystemMediaTransportControlsSession session, TimelinePropertiesChangedEventArgs e)
        { 

            if (!session.SourceAppUserModelId.ToLower().Contains("spotify") || currentGlobalSession.GetHashCode() != session.GetHashCode())
                return;

            GlobalSystemMediaTransportControlsSessionTimelineProperties timeline = session.GetTimelineProperties();
            var mediaProperties = session.TryGetMediaPropertiesAsync().GetAwaiter().GetResult();
            double currentTime = timeline.LastUpdatedTime.TimeOfDay.TotalMilliseconds;

            if (currentTrackName == null && currentArtist == null)
            {
                currentArtist = mediaProperties.Artist;
                currentTrackName = mediaProperties.Title;
                currentAlbum = mediaProperties.AlbumTitle;
                currentLength = timeline.EndTime.TotalMilliseconds;
                currentListeningTime = 0;
                startListeningTime = currentTime;
            }
        }

        private void callbackPlaybackStatus(GlobalSystemMediaTransportControlsSession session, PlaybackInfoChangedEventArgs e)
        { 

            if (!session.SourceAppUserModelId.ToLower().Contains("spotify") || currentGlobalSession.GetHashCode() != session.GetHashCode())
                return;

            GlobalSystemMediaTransportControlsSessionPlaybackStatus status = session.GetPlaybackInfo().PlaybackStatus;
            double currentTime = session.GetTimelineProperties().LastUpdatedTime.TimeOfDay.TotalMilliseconds;


            if (status == GlobalSystemMediaTransportControlsSessionPlaybackStatus.Playing)
            {
                Console.WriteLine("Playing ");

                /*if(!songPaused && startListeningTime != 0)
                {
                    currentListeningTime += currentTime - startListeningTime;
                }
                */
                startListeningTime = currentTime;
                songPaused = false;
            }
            else if (status == GlobalSystemMediaTransportControlsSessionPlaybackStatus.Paused)
            {

                if (startListeningTime != 0)
                {
                    currentListeningTime += currentTime - startListeningTime;
                }
                Console.WriteLine("Paused, listed for: " + currentListeningTime);
                songPaused = true;
            } 
            else if (status == GlobalSystemMediaTransportControlsSessionPlaybackStatus.Closed)
            {
                Console.WriteLine("Closed");
            }
            else if (status == GlobalSystemMediaTransportControlsSessionPlaybackStatus.Stopped)
            {
                Console.WriteLine("Stopped");

                double preUpdateListeningTime = currentListeningTime;

                if (!songPaused && startListeningTime != 0)
                    currentListeningTime += currentTime - startListeningTime;

                if (currentListeningTime > currentLength + 2000 + currentLength / 2)
                    currentListeningTime = preUpdateListeningTime;

                if (currentListeningTime > 10000 && currentListeningTime < currentLength + 2000 + currentLength / 2 && serviceRunning)
                {
                    Console.WriteLine("Insert2 into db " + currentTrackName + " " + currentListeningTime);
                    database.insertData(currentTrackName, currentArtist, ((int)Math.Round(currentListeningTime)).ToString());
                     
                    updateStatusBar(currentTrackName + " by " + currentArtist);
                }
                else
                    Console.WriteLine("Insert2 skipped");
            }
        }
        private void callbackMediaProperties(GlobalSystemMediaTransportControlsSession session, MediaPropertiesChangedEventArgs e)
        {
            if (!session.SourceAppUserModelId.ToLower().Contains("spotify") || currentGlobalSession.GetHashCode() != session.GetHashCode())
                return;

            GlobalSystemMediaTransportControlsSessionTimelineProperties timeline = session.GetTimelineProperties();
            var mediaProperties = session.TryGetMediaPropertiesAsync().GetAwaiter().GetResult();
            double currentTime = timeline.LastUpdatedTime.TimeOfDay.TotalMilliseconds;

            if ((mediaProperties.Artist != currentArtist || currentTrackName != mediaProperties.Title) && session.SourceAppUserModelId.ToLower().Contains("spotify"))
            {
                //Salvo quella vecchia 
                Console.WriteLine("New Track");

                double preUpdateListeningTime = currentListeningTime;

                if (!songPaused && startListeningTime != 0)
                    currentListeningTime += currentTime - startListeningTime;

                if (currentListeningTime > currentLength + 2000 + currentLength / 2)
                    currentListeningTime = preUpdateListeningTime;

                if (currentListeningTime > 10000 && currentListeningTime < currentLength + 2000 + currentLength / 2 && session.SourceAppUserModelId.ToLower().Contains("spotify") && serviceRunning)
                {
                    Console.WriteLine("Insert1 into db " + currentTrackName + " " + (int)Math.Round(currentListeningTime));
                    database.insertData(currentTrackName, currentArtist, ((int)Math.Round(currentListeningTime)).ToString());

                    updateStatusBar(currentTrackName + " by " + currentArtist);
                }
                else
                    Console.WriteLine("Insert1 skipped");

                currentArtist = mediaProperties.Artist;
                currentTrackName = mediaProperties.Title;
                currentAlbum = mediaProperties.AlbumTitle;
                currentLength = timeline.EndTime.TotalMilliseconds;
                currentListeningTime = 0;
                startListeningTime = currentTime;
            }
        }
    }
}
