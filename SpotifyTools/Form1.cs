﻿using Microsoft.Win32;
using SpotifyTools.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Windows.Media.Control; 

namespace SpotifyTools
{
    
    public partial class Form1 : Form
    {
          
        public Form1()
        {
            InitializeComponent();
             
            notifyIcon1.ContextMenuStrip = contextMenuStrip1;
            showToolStripMenuItem.Click += showToolStripMenuItem_Click;
            exitToolStripMenuItem.Click += exitToolStripMenuItem_Click;
             
            webServer = new WebServer();
            database = new DatabaseManager();
            rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            portNumber = Settings.Default.portNumber; 
            if(portNumber=="")
            {
                portNumber = "5050";
                Settings.Default.portNumber = "5050";
                Settings.Default.Save();
            }

            String currentValue = (String)rk.GetValue("SpotifyTools");

            //Inizio il server se è impostato at startup
            if (currentValue != null)
            {
                allowVisible = false;
                bool connResult = database.createConnection();
                if (connResult)
                {
                    startServer(); 
                }
                else
                {
                    toolStripStatusLabel1.Text = "Error has occurred during database initialization.";
                    btnStartServer.Enabled = false;
                    btnStopServer.Enabled = false;
                }
                database.closeConnection();
            }
            else
            {
                allowVisible = true;
            }

            Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
            // use 'Screen.AllScreens[1].WorkingArea' for secondary screen
            this.Left = workingArea.Left + workingArea.Width - this.Size.Width;
            this.Top = workingArea.Top + workingArea.Height - this.Size.Height;

        }
         

        private bool allowVisible;     // ContextMenu's Show command used
        private bool allowClose;       // ContextMenu's Exit command used
        WebServer webServer;
        DatabaseManager database; 
        String portNumber = "5050";
        RegistryKey rk;
        CancellationTokenSource wtoken;
        Task task;


        protected override void SetVisibleCore(bool value)
        {
            if (!allowVisible)
            {
                value = false;
                if (!this.IsHandleCreated) CreateHandle();
            }
            base.SetVisibleCore(value);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (!allowClose)
            {
                this.Hide();
                e.Cancel = true;
            }
            base.OnFormClosing(e);
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            allowVisible = true;
            updateView();
            Show();
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            allowVisible = true;
            updateView();
            Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            allowClose = true; 
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /*Console.WriteLine("Started");
            var spotify = Process.GetProcessesByName("Spotify");
            foreach (var song in spotify)
            {
                Console.WriteLine(song.MainWindowTitle);
            }*/

            updateView();
        }

        private void updateView()
        { 
            updateServerStatus();
            txtPort.Text = portNumber;
            txtServerAddress.Text = "Local server address: " + getLocalIP();
            loadNotDownloadAmount();

            String currentValue = (String)rk.GetValue("SpotifyTools");
            if (currentValue == null)
                checkBox1.Checked = false;
            else
                checkBox1.Checked = true; 
        }



        private String getLocalIP()
        {
            string localIP = "127.0.0.1";
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                localIP = endPoint.Address.ToString();
            }
            return localIP;
        } 

        private void btnStartServer_Click(object sender, EventArgs e)
        { 
            bool isPortNumber = int.TryParse(txtPort.Text, out int n);
            if (!isPortNumber || txtPort.Text.Length != 4)
            {
                MessageBox.Show("Port is not a valid number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                return;
            }
            else
                portNumber = txtPort.Text;

            Properties.Settings.Default["PortNumber"] = portNumber;
            Properties.Settings.Default.Save();

            startServer();

            updateServerStatus(); 
        }

        private void updateServerStatus()
        { 
            if (webServer.serverStatus())
            {
                lblServerStatus.ForeColor = Color.DarkGreen;
                lblServerStatus.Text = "Online";
                txtPort.Enabled = false;

                btnStartServer.Enabled = false;
                btnStopServer.Enabled = true;
            } 
            else
            {
                lblServerStatus.ForeColor = Color.DarkRed;
                lblServerStatus.Text = "Oflline";
                txtPort.Enabled = true;

                btnStartServer.Enabled = true;
                btnStopServer.Enabled = false;
            } 
        }

        private void startServer()
        {
            bool serverResult = false;
            if (!webServer.serverStatus())
                serverResult = webServer.startWebServer("http://*:" + portNumber + "/");

            if (!serverResult)
                MessageBox.Show("Run SpotifyTools as Administrator to enable the server in this computer or if you are changing the port.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                wtoken = new CancellationTokenSource();
                //Task.Factory.StartNew(() => startMediaListener());

                task = Task.Factory.StartNew(() =>
                {
                    MediaListener mediaListener = new MediaListener(result => toolStripStatusLabel1.Text = "Last saved: " + result);
                    mediaListener.startMediaListener();
                    wtoken.Token.Register(mediaListener.stopMediaListener);

                }, wtoken.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default); 
            } 
        }
         
        private void btnStopServer_Click(object sender, EventArgs e)
        {
            try
            { 
                //mediaListener.stopMediaListener();
                webServer.stopWebServer();
                updateServerStatus();
                wtoken.Cancel();
            }
            catch { }
        }

        private void loadNotDownloadAmount()
        {
            bool connResult = database.createConnection();

            if (connResult)
            {
                List<String> values = new List<String>();

                long millis = database.readMillisNotDownloaded();

                long days = millis / (24 * 60 * 60 * 1000);
                long daysms = millis % (24 * 60 * 60 * 1000);

                long hours = daysms / (60 * 60 * 1000);
                long hoursms = millis % (60 * 60 * 1000);

                long minutes = hoursms / (60 * 1000);

                txtTimeNotSynced.Text = days.ToString() + "d " + hours.ToString() + "h " + minutes.ToString() + "m";

                database.closeConnection();
            }
            else txtTimeNotSynced.Text = "0d 0h 0m"; 
        }
         

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey
            ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            String currentValue = (String)rk.GetValue("SpotifyTools");
             
            if (checkBox1.Checked)
                rk.SetValue("SpotifyTools", Application.ExecutablePath);
            else
                rk.DeleteValue("SpotifyTools", false);
        }
    }
}
