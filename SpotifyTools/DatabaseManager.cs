﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyTools
{
    class DBRecord
    {
        [JsonProperty("windowsId")]
        String windowsId;

        [JsonProperty("trackName")]
        String trackName;

        [JsonProperty("artistName")]
        String artistName;

        [JsonProperty("listeningTime")]
        String listeningTime;

        [JsonProperty("listeningDate")]
        String listeningDate;

        public DBRecord(String id, String track, String artist, String time, String date)
        {
            this.windowsId = id;
            this.trackName = track;
            this.artistName = artist;
            this.listeningTime = time;
            this.listeningDate = date;
        }
    }

    class DatabaseManager
    {
        private SQLiteConnection sqlite_conn;
        public DatabaseManager()
        {
            
        }

        public bool createConnection()
        { 
            sqlite_conn = initConnection();
            if (sqlite_conn != null)
            {
                createTable(); 
                return true;
            }
            else
                return false;
        }

        private SQLiteConnection initConnection()
        {
            var directory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string spotifyToolsPath = Path.Combine(directory, "SpotifyTools");
            string dbPath = Path.Combine(directory, "SpotifyTools", "spotifyTools.db");
            string connectionString = string.Format("Data Source={0}", dbPath).Replace(@"\\",@"\");

            if (!Directory.Exists(spotifyToolsPath))
            {
                Directory.CreateDirectory(spotifyToolsPath);
            }

            // Create a new database connection:
            SQLiteConnection sqlite_connection = new SQLiteConnection(connectionString);
            // Open the connection:
            try
            {
                sqlite_connection.Open(); 
            }
            catch (Exception ex)
            {
                return null;
            }
            return sqlite_connection;
        }

        public void closeConnection()
        {
            sqlite_conn.Close();
        }

        private void createTable()
        { 
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "CREATE TABLE IF NOT EXISTS songs (ID INTEGER PRIMARY KEY, SONG_ID TEXT, SONG_NAME TEXT, SONG_ARTIST TEXT, LISTENED_TIME INTEGER, DATE TEXT, DOWNLOAD_DATE TEXT);";
            sqlite_cmd.ExecuteNonQuery(); 
        }

        public void insertData(String songName, String artistName, String listenedTime)
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();   
            sqlite_cmd.CommandText = "INSERT INTO songs (SONG_NAME, SONG_ARTIST, LISTENED_TIME, DATE) VALUES (@param1,@param2," + listenedTime + ",datetime('now', 'localtime'));";
            sqlite_cmd.CommandType = CommandType.Text;
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@param1", songName));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@param2", artistName));
            sqlite_cmd.ExecuteNonQuery();
        }

        public List<DBRecord> readAllData()
        { 
            List<DBRecord> records = new List<DBRecord>();

            if (sqlite_conn == null || sqlite_conn.State == ConnectionState.Closed)
                return records;

            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM songs";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {
                String key = sqlite_datareader.GetInt32(0).ToString();
                bool id = sqlite_datareader.IsDBNull(1);
                String trackName = sqlite_datareader.GetString(2);
                String artistName = sqlite_datareader.GetString(3);
                String listenedTime = sqlite_datareader.GetInt64(4).ToString();  
                String date = sqlite_datareader.GetString(5);

                bool isDownloadDateNull = sqlite_datareader.IsDBNull(6);

                String downloadDate = "empty";
                if(!isDownloadDateNull)
                    downloadDate = sqlite_datareader.GetString(6);

                records.Add(new DBRecord(key, trackName, artistName, listenedTime, date));
                Console.WriteLine(key + " "+ id + " "+ trackName + " "+ artistName + " "+ artistName + " "+ listenedTime + " " + date + " " + downloadDate);
            }

            return records;
        }

        public List<DBRecord> readNotDownloaded()
        {
            List<DBRecord> records = new List<DBRecord>();

            if (sqlite_conn == null || sqlite_conn.State == ConnectionState.Closed)
                return records;

            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM songs WHERE DOWNLOAD_DATE IS NULL;";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {
                String key = sqlite_datareader.GetInt32(0).ToString();
                bool id = sqlite_datareader.IsDBNull(1);
                String trackName = sqlite_datareader.GetString(2);
                String artistName = sqlite_datareader.GetString(3);
                String listenedTime = sqlite_datareader.GetInt64(4).ToString();
                String date = sqlite_datareader.GetString(5);

                bool isDownloadDateNull = sqlite_datareader.IsDBNull(6);

                String downloadDate = "empty";
                if (!isDownloadDateNull)
                    downloadDate = sqlite_datareader.GetString(6);

                records.Add(new DBRecord(key, trackName, artistName, listenedTime, date));
                Console.WriteLine(key + " " + id + " " + trackName + " " + artistName + " " + artistName + " " + listenedTime + " " + date + " " + downloadDate);
            }

            return records;
        }

        public void updateDownloadDate()
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "UPDATE songs SET DOWNLOAD_DATE=datetime('now', 'localtime') WHERE DOWNLOAD_DATE IS NULL;";
            sqlite_cmd.ExecuteNonQuery();
        }

        public long readMillisNotDownloaded()
        {
            if (sqlite_conn==null || sqlite_conn.State == ConnectionState.Closed)
                return 0;

            SQLiteCommand sqlite_cmd;
            SQLiteDataReader sqlite_datareader;

            long value = 0;

            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT SUM(LISTENED_TIME) FROM songs WHERE DOWNLOAD_DATE IS NULL;";
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                if (sqlite_datareader.IsDBNull(0))
                    value = 0;
                else
                    value = sqlite_datareader.GetInt64(0);
            }

            return value;
        }
    }
}
