﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NetFwTypeLib;

namespace SpotifyTools
{
    public class WebServer
    {
        //netsh http add urlacl url=http://*:5050/ user="Everyone"
        HttpListener listener;
        //Thread backgroundThreadListner;

        DatabaseManager database;


        CancellationTokenSource wtoken;

        public WebServer()
        {
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }
            database = new DatabaseManager();
            // Create a listener.
            listener = new HttpListener();
        } 

        public void stopWebServer()
        { 
            //backgroundThreadListner.Abort();
            listener.Stop();
            database.closeConnection();
            if (listener.IsListening)
            {
                listener.Stop(); 
                wtoken.Cancel();
                Console.WriteLine("Listener stopped");
            }
        }

        public bool serverStatus()
        {
            if (listener == null)
                return false;
            return listener.IsListening;
        }

        private bool checkNetShPermission(string prefix)
        {
            string args = @"http show urlacl"; 

            Process process = new Process();
            process.StartInfo.FileName = "netsh.exe";
            process.StartInfo.Arguments = args;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            //* Read the output (or the error)
            string output = process.StandardOutput.ReadToEnd();
            if (output.Contains(prefix))
                return true; 
             
            process.WaitForExit(); 

            return false;
        }

        public bool startWebServer(string prefixes, bool forceStart=false)
        {
            
            // Add the prefixes.
            bool httpPermission = checkNetShPermission(prefixes);

            //Load firewall permission
            INetFwRule firewallRule = (INetFwRule)Activator.CreateInstance(
                    Type.GetTypeFromProgID("HNetCfg.FWRule"));

            INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(
            Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));

            bool firewallFound = false;
            foreach (INetFwRule rule in firewallPolicy.Rules)
            {
                if (rule.Name.Equals("SpotifyTools")) firewallFound = true;
            }

            if ((httpPermission && !listener.IsListening && firewallFound) || forceStart)
            {
                try
                {
                    listener.Prefixes.Add(prefixes);

                    listener.Start();

                    Console.WriteLine("Listening...");

                    wtoken = new CancellationTokenSource();
                    //Task.Factory.StartNew(() => startMediaListener()); 

                    database.createConnection();

                    Task task = Task.Factory.StartNew(async () =>
                    {
                        while (true) { 
                            await Listen(listener);
                            try
                            { 
                                wtoken.Token.ThrowIfCancellationRequested();
                            }
                            catch (OperationCanceledException ex)
                            {
                                Console.WriteLine("Cancel request");
                                break;
                            }
                        }

                    }, wtoken.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);

                    return true;
                }
                catch
                {
                    return false;
                }
                /*backgroundThreadListner = new Thread(new ThreadStart(async () =>
                {
                    while (true)
                    {
                        // Note: The GetContext method blocks while waiting for a request.
                        HttpListenerContext context = listener.GetContext();
                        HttpListenerRequest request = context.Request;

                        // Obtain a response object.
                        HttpListenerResponse response = context.Response;
                        // Construct a response.
                        string responseString = "";
                         
                        if (request.Url.AbsolutePath.Contains("close"))
                        {
                            stopWebServer();
                            break;
                        }
                        else if (request.Url.AbsolutePath.Contains("sync"))
                        {
                            List<DBRecord> records = database.readNotDownloaded();
                            responseString = await Task.Run(() => JsonConvert.SerializeObject(records.AsEnumerable()));
                        }
                        else if (request.Url.AbsolutePath.Contains("setDownloaded"))
                        {
                            database.updateDownloadDate();
                            responseString = "Done";
                        }
                        else
                        {
                            responseString = "SpotifyTools - Windows - v1.0.0";
                        }

                        byte[] buffer = UTF8Encoding.UTF8.GetBytes(responseString);
                        // Get a response stream and write the response to it.
                        response.ContentLength64 = buffer.Length;
                        response.ContentEncoding = Encoding.UTF8;
                        response.ContentType = "application/json; charset=utf-8";


                        Stream output = response.OutputStream;
                        output.Write(buffer, 0, buffer.Length);
                        // You must close the output stream.
                        output.Close();
                    }

                }));*/
                //backgroundThreadListner.IsBackground = true;
                //backgroundThreadListner.Start();

            }
            else
            {
                //netsh http add urlacl url=http://*:5050/ user="Everyone"

                try
                {
                    //HTTP Permission
                    if(!httpPermission)
                    {
                        var sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                        var account = (NTAccount)sid.Translate(typeof(NTAccount));

                        string args = "http add urlacl url=" + prefixes + " user=\"" + account.Value + "\"";

                        Process process = new Process();
                        process.StartInfo.FileName = "netsh.exe";
                        //process.StartInfo.Verb = "runas";
                        process.StartInfo.Arguments = args;
                        process.StartInfo.UseShellExecute = true;
                        process.Start();

                        process.WaitForExit();
                    } 

                    //Firewall Permission  
                    if(!firewallFound)
                    {
                        firewallRule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
                        firewallRule.Description = "SpotifyTools server";
                        firewallRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN; // inbound
                        firewallRule.Enabled = true;
                        firewallRule.InterfaceTypes = "All";
                        firewallRule.Name = "SpotifyTools";
                        firewallPolicy.Rules.Add(firewallRule);
                    }
                    return startWebServer(prefixes, true);
                }
                catch {
                    return false;
                } 
            } 
        }

        private async Task Listen(HttpListener l)
        {
            try
            {
                var context = await l.GetContextAsync();

                HttpListenerRequest request = context.Request;

                // Obtain a response object.
                HttpListenerResponse response = context.Response;
                // Construct a response.
                string responseString = "";

                if (request.Url.AbsolutePath.Contains("close"))
                {
                    stopWebServer(); 
                }
                else if (request.Url.AbsolutePath.Contains("sync"))
                {
                    List<DBRecord> records = database.readNotDownloaded();
                    responseString = await Task.Run(() => JsonConvert.SerializeObject(records.AsEnumerable()));
                }
                else if (request.Url.AbsolutePath.Contains("setDownloaded"))
                {
                    database.updateDownloadDate();
                    responseString = "Done";
                }
                else
                {
                    responseString = "SpotifyTools - Windows - v1.0.0";
                }

                byte[] buffer = UTF8Encoding.UTF8.GetBytes(responseString);
                // Get a response stream and write the response to it.
                response.ContentLength64 = buffer.Length;
                response.ContentEncoding = Encoding.UTF8;
                response.ContentType = "application/json; charset=utf-8";


                Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                // You must close the output stream.
                output.Close(); 
            }
            catch (HttpListenerException)
            {
                Console.WriteLine("screw you guys, I'm going home!");
            }
        } 
    }
}
